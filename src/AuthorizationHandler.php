<?php

namespace Drupal\basicshib;

use Drupal\basicshib\Plugin\BasicShibPluginManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\user\UserInterface;

/**
 *
 */
class AuthorizationHandler implements AuthorizationHandlerInterface
{

    use GrouperHelperTrait;

    /**
     * Grouper plugin instance variable. 
     * 
     * @var GrouperPluginInterface
     */
    private $_grouper;

    /**
     * AuthorizationHandler constructor.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
     * @param \Drupal\basicshib\Plugin\BasicShibPluginManager $_grouper_plugin_manager
     *
     * @throws \Drupal\Component\Plugin\Exception\PluginException
     */
    public function __construct(ConfigFactoryInterface $config_factory,
        RequestStack $request_stack,
        BasicShibPluginManager $grouper_plugin_manager
    ) {

        $this->session_tracker = new SessionTracker(
            $request_stack->getCurrentRequest()->getSession()
        );

        $plugins = $config_factory
            ->get('basicshib.settings')
            ->get('plugins');

        // Create instance of grouper plugin.
        $this->_grouper = $grouper_plugin_manager
            ->createInstance($plugins['grouper']);
    }

    /**
     * @param \Drupal\user\Entity\UserInterface $current_account
     *
     * @inheritDoc
     * 
     * @return a new role. 
     */
    public function authorize(UserInterface $current_account)
    {
  
        // Logs the HTTP request that is user-specific 
        //at the time of current login session.
        $user_groups = explode(';', $this->getUserAttribute('ismemberof'));
        $previous_roles = $current_account->getRoles();

        // Get map from GrouperPluginDefault.
        $group_role_map = $this->_grouper->getMap();

        // Remove previous user roles.
        foreach ($previous_roles as $role_id) {
            if (strcmp($role_id, 'administrator') !== 0) {
                $current_account->removeRole($role_id);
            }
        }

        // Assign a role to the current account if user group is mapped to that role.
        foreach ($group_role_map as $group => $role) {
            if (!empty($group) && in_array($group, $user_groups)) {
                $current_account->addRole($role);
            }
        }
        $current_account->save();
    }

}
