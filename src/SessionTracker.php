<?php

namespace Drupal\basicshib;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class SessionTracker.
 *
 * Tracks the shibboleth session id that was used for login.
 *
 * @package Drupal\basicshib
 */
class SessionTracker
{
    const VARNAME = 'basicshib_tracked$_session_id';

    /**
     * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
     */
    private $_session;

    /**
     * SessionTracker constructor.
     *
     * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $_session
     */
    public function __construct(SessionInterface $session = null) 
    {
        $this->_session = $session;
    }

    /**
     * Get the session id.
     *
     * @return string|null
     *   The tracked session id.
     */
    public function get() 
    {
        return $this->_session !== null
        ? $this->_session->get(self::VARNAME)
        : null;
    }

    /**
     * Set the session id.
     *
     * @param string|null $value
     * The value to set, or NULL to remove the session entry.
     */
    public function set($value) 
    {
        if ($this->_session !== null) {
            $this->_session->set(self::VARNAME, $value);
        }
    }

    /**
     * Clear the tracked session id.
     */
    public function clear() 
    {
        if ($this->_session) {
            $this->_session->remove(self::VARNAME);
        }
    }

    /**
     * @return bool
     */
    public function exists() 
    {
        if ($this->_session) {
            return $this->_session->has(self::VARNAME);
        }
    }

}
