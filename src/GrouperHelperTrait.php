<?php

namespace Drupal\basicshib;

use Drupal\user\Entity\Role;

/**
 *
 */
trait GrouperHelperTrait
{

    /**
     * Queries available Drupal roles from the specific website.
     * returns an array with the Drupal roles excluding 'anonymous' and 'authenticated'.
     *
     * @return array_map
     */
    protected static function getDrupalRoles()
    {
        $array = array_keys(Role::loadMultiple());
        $roles = [];
        foreach ($array as $role) {
            if (strcmp($role, 'anonymous') != 0 
                && strcmp($role, 'authenticated') != 0
            ) {
                array_push($roles, $role);
            }
        }
        return $roles;
    }

    /**
     * Returns a user attribute specified by a string.
     *
     * @param string
     *   denotes the desired attribute name
     *
     * @return string
     */
    protected function getUserAttribute($attribute_name)
    {
        $req = \Drupal::request();
        return $req->server->get($attribute_name, null);
    }

}
