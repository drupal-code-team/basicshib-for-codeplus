<?php
/**
 * BasicShibUserProvider File Doc Comment
 *
 * @category BasicShibUserProvider
 *
 */
namespace Drupal\basicshib\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class BasicShibUserProvider.
 *
 * @package Drupal\basicshib\Annotation
 *
 * @Annotation
 */
class BasicShibUserProvider extends Plugin
{
    /**
     * Machine name of the plugin.
     * 
     * @var string
     */
    public $id;

    /**
     * Human-readable name of the plugin.
     * 
     * @var string
     */
    public $name;
    
}
