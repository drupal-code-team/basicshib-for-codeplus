<?php
/**
 * BasicShibAuthFilter File Doc Comment
 *
 * @category BasicShibAuthFilter
 *
 */
namespace Drupal\basicshib\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class AuthenticationFilter.
 *
 * @package Drupal\basicshib\Annotation
 *
 * @Annotation
 */
class BasicShibAuthFilter extends Plugin
{
    /**
     * Machine name of the plugin.
     * 
     * @var string
     */
    public $id;

    /**
     * Human-readable name of the plugin.
     * 
     * @var string
     */
    public $name;

}
