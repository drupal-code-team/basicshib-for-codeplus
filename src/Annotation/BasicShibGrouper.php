<?php
/**
 * BasicShibGrouper File Doc Comment
 *
 * @category BasicShibGrouper
 *
 */
namespace Drupal\basicshib\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class BasicShibGrouper.
 *
 * @package Drupal\basicshib\Annotation
 *
 * @Annotation
 */
class BasicShibGrouper extends Plugin
{
    /**
     * Machine name of the plugin.
     *
     * @var string
     */
    public $id;
    /**
     * Human-readable name of the plugin.
     *
     * @var string
     */
    public $name;

}
