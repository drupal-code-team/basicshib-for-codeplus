<?php
/**
 * RequestEventSubscriber File Doc Comment
 *
 * @category BasicShibGrouper
 *
 */
namespace Drupal\basicshib\EventSubscriber;

use Drupal\basicshib\AuthenticationHandlerInterface;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Created by PhpStorm.
 * User: th140
 * Date: 10/19/17
 * Time: 1:35 PM
 */
class RequestEventSubscriber implements EventSubscriberInterface
{

    /**
     * Instance variable representing the current user.
     *  
     * @var \Drupal\Core\Session\AccountProxyInterface
     */
    private $_current_user;

    /**
     * Instance variable representing th elogger channel factory
     * 
     * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
     */
    private $_logger_channel_factory;

    /**
     * Instance variable representing the authentication handler
     * 
     * @var \Drupal\basicshib\AuthenticationHandlerInterface
     */
    private $_authentication_handler;

    /**
     * AuthEventSubscriber constructor.
     * 
     * @param \Drupal\Core\Session\AccountProxyInterface $_current_user
     * @param LoggerChannelFactoryInterface $_logger_channel_factory
     * @param \Drupal\basicshib\AuthenticationHandlerInterface $_authentication_handler  
     */
    public function __construct(AccountProxyInterface $_current_user,
        LoggerChannelFactoryInterface $_logger_channel_factory,
        AuthenticationHandlerInterface $_authentication_handler              
    ) {

        $this->_current_user = $_current_user;
        $this->_logger_channel_factory = $_logger_channel_factory;
        $this->_authentication_handler = $_authentication_handler;
    }

    /**
     * {@inheritdoc}
     * 
     * @return list of events. 
     */
    public static function getSubscribedEvents() 
    {
        $events[KernelEvents::REQUEST][] = ['onRequest', 100];
        return $events;
    }

    /**
     * Request handler.
     *
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     * @param $eventId
     * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $dispatcher
     *
     * @throws \InvalidArgumentException
     */
    public function onRequest(GetResponseEvent $event, $eventId, 
        ContainerAwareEventDispatcher $dispatcher
    ) {
        $this->_authentication_handler->checkUserSession(
            $event->getRequest(),
            $this->_current_user
        );
    }

}
