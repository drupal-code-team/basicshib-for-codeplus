<?php

namespace Drupal\basicshib;

use Drupal\basicshib\Exception\AttributeException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 *
 */
class AttributeMapper implements AttributeMapperInterface
{
    /**
     * @var \Drupal\Core\Config\ImmutableConfig
     */
    private $_configuration;

    /**
     * @var \Symfony\Component\HttpFoundation\ServerBag
     */
    private $_server;

    /**
     * @var array
     */
    private $_attribute_map = [];

    /**
     * AttributeMapper constructor.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
     */
    public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack)
    {
        $this->_configuration = $config_factory
            ->get('basicshib.settings');

        $this->_server = $request_stack
            ->getCurrentRequest()
            ->server;

        $this->_attribute_map = $this->_getAttributeMap();
    }

    /**
     * Get an attribute's value.
     *
     * @param $id
     *   The id of the attribute to fetch.  An exception is thrown if no mapping
     *   exists for the provided id.
     *
     * @param bool $empty_allowed
     *   Whether to allow empty attributes. When false, an exception is thrown if
     *   the attribute is not set.
     *
     * @return string
     *   The value of the attribute
     *
     * @throws \Drupal\basicshib\Exception\AttributeException
     *
     * @todo Remove $empty_allowed and check this somewhere else.
     */
    public function getAttribute($id, $empty_allowed = false)
    {
        if (isset($this->_attribute_map[$id])) {
            $def = $this->_attribute_map[$id];
            $value = $this->_server->get($def['name']);
            if (!$value && !$empty_allowed) {
                throw new AttributeException(
                    sprintf(
                        'Attribute is not set: \'%s\' (mapped to \'%s\')',
                        $def['name'], $id
                    ), 
                    AttributeException::NOT_SET
                );
            }
            return $value;
        }
        throw new AttributeException(
            sprintf(
                'Key attribute is not mapped: \'%s\'', $id
            ), 
            AttributeException::NOT_MAPPED
        );
    }

    /**
     * @return array
     *
     * @throws \Drupal\basicshib\Exception\AttributeException
     */
    private function _getAttributeMap()
    {
        $config = $this->_configuration
            ->get('attribute_map');

        foreach ($config['key'] as $id => $name) {
            $this->_attribute_map[$id] = [
              'id' => $id,
              'name' => $name,
              'key' => true,
            ];
        }

        foreach ($config['optional'] as $def) {
            if (isset($this->_attribute_map[$def['id']])) {
                throw new AttributeException(
                    sprintf(
                        'Atrribute with id \'%s\' is already defined',
                        $def['id']
                    ), AttributeException::DUPLICATE_ID
                );
            }
            $this->_attribute_map[$def['id']] = [
              'id' => $def['id'],
              'name' => $def['name'],
              'key' => false,
            ];
        }
        return $this->_attribute_map;
    }
}
