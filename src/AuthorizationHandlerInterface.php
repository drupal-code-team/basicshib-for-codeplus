<?php

namespace Drupal\basicshib;

use Drupal\user\UserInterface;

/**
 *
 */
interface AuthorizationHandlerInterface
{

    /**
     * Attempt to authorize, granting users roles according to their Grouper groups.
     *
     * @throws AuthorizationException
     */
    public function authorize(UserInterface $current_account);

}
