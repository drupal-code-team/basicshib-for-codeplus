<?php

namespace Drupal\basicshib\Plugin\basicshib\grouper;

use Drupal\basicshib\Plugin\GrouperPluginInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\basicshib\GrouperHelperTrait;

/**
 * Class GrouperPluginDefault.
 *
 * @package Drupal\basicshib\Plugin\basicshib\grouper
 *
 * @BasicShibGrouper(
 *   id = "grouper_default",
 *   title = "grouper"
 * )
 */
class GrouperPluginDefault implements GrouperPluginInterface, ContainerFactoryPluginInterface
{
    use GrouperHelperTrait;

    /**
     * Instance variable configuration
     * 
     * @var \Drupal\Core\Config\ImmutableConfig
     */
    private $_configuration;

    /**
     * Group to Role map.
     */
    private $_map;

    /**
     * GrouperPluginDefault constructor.
     *
     * @param \Drupal\Core\Config\ImmutableConfig $_configuration
     */
    public function __construct(ImmutableConfig $configuration)
    {
        $this->_configuration = $configuration;
        $this->_map = [];
    }

    /**
     * {@inheritDoc}
     * @param ContainerInterface $container
     * @param array $configuration
     * @param String $plugin_id
     * @param String $plugin_definition 
     * 
     * @return GrouperDeafultPlugin instance
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $container->get('config.factory')->get('basicshib.grouper_settings')
        );
    }

    /**
     * {@inheritDoc}
     * 
     * @return array_map with keys as grouper groups and values as Drupal roles. 
     */
    public function getMap()
    {
        $role_map = null;
        $drupal_roles = $this->getDrupalRoles();
        for ($i = 0; $i < count($drupal_roles); $i++) {
            $role = $this->_configuration->get('role_' . $i);
            $key = $drupal_roles[$i];
            $role_map['' . $key] = explode(';', $role);
        }

        $role_map_keys = array_keys($role_map);
        for ($i = 0; $i < count($role_map_keys); $i++) {
            $roles = $role_map_keys[$i];
            for ($j = 0; $j < count($role_map[$roles]); $j++) {
                $group_path = $role_map[$roles][$j];
                $this->_map[$group_path] = $roles;
            }
        }
        return $this->_map;
    }
}
