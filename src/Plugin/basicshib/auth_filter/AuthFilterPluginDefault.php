<?php

namespace Drupal\basicshib\Plugin\basicshib\auth_filter;

use Drupal\basicshib\AuthenticationHandler;
use Drupal\basicshib\Plugin\AuthFilterPluginInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AuthenticationFilterPluginDefault.
 *
 * @package Drupal\basicshib\Plugin\basicshib\auth_filter
 *
 * @BasicShibAuthFilter(
 *   id = "basicshib",
 *   title = "Basic authentication filter"
 * )
 */
class AuthFilterPluginDefault implements AuthFilterPluginInterface, ContainerFactoryPluginInterface
{
    /**
     * @var \Drupal\Core\Config\ImmutableConfig
     */
    private $_configuration;

    /**
     * AuthenticationFilterPluginDefault constructor.
     *
     * @param \Drupal\Core\Config\ImmutableConfig $_configuration
    
    */
    public function __construct(ImmutableConfig $configuration)
    {
        $this->_configuration = $configuration;
    }

    /**
     * @inheritDoc
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $container->get('config.factory')->get('basicshib.auth_filter')
        );
    }

    /**
     * @inheritDoc
     */
    public function isUserCreationAllowed()
    {
        $create_filter = $this->_configuration
            ->get('create');
        return $create_filter['allow'];
    }

    /**
     * @inheritDoc
     */
    public function isExistingUserLoginAllowed(UserInterface $account)
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getError($code, UserInterface $account = null)
    {
        switch ($code) {
        case self::ERROR_CREATION_NOT_ALLOWED:
            $create_filter = $this->_configuration
                ->get('create');
            return $create_filter['error'];
        }
    }

    /**
     * @inheritDoc
     */
    public function checkSession(Request $request, AccountProxyInterface $account)
    {
        return AuthenticationHandler::AUTHCHECK_IGNORE;
    }
}
