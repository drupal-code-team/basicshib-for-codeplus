<?php

namespace Drupal\basicshib\Plugin\basicshib\user_provider;

use Drupal\basicshib\Plugin\UserProviderPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserProviderPluginDefault.
 *
 * @package Drupal\basicshib\Plugin\basicshib\user_provider
 *
 * @BasicShibUserProvider(
 *   id = "basicshib",
 *   title = "Default user provider"
 * )
 */
class UserProviderPluginDefault implements UserProviderPluginInterface, ContainerFactoryPluginInterface
{
    /**
     * @var \Drupal\user\UserStorageInterface
     */
    private $_user_storage;

    /**
     * @inheritDoc
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $container->get('entity_type.manager')->getStorage('user')
        );
    }

    /**
     * DefaultUserProvider constructor.
     *
     * @param \Drupal\user\UserStorageInterface $_user_storage
     */
    public function __construct(UserStorageInterface $user_storage)
    {
        $this->_user_storage = $user_storage;
    }

    /**
     * @inheritDoc
     */
    public function loadUserByName($name)
    {
        $users = $this->_user_storage
            ->loadByProperties(['name' => $name]);

        if (count($users) === 1) {
            return reset($users);
        }
    }

    /**
     * @inheritDoc
     */
    public function createUser($name, $mail)
    {
        return $this->_user_storage
            ->create(
                [
                'name' => $name,
                'mail' => $mail,
                'status' => 1,
                ]
            );
    }
}
