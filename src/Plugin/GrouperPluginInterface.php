<?php

namespace Drupal\basicshib\Plugin;

/**
 *
 */
interface GrouperPluginInterface
{
    /**
     * Returns a map with key values of grouper groups and values of Drupal roles.
     *
     * This information is managed by the website developer.
     *
     * @return map
     */
    public function getMap();
}
