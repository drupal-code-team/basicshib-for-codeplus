<?php

namespace Drupal\basicshib\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\basicshib\GrouperHelperTrait;

/**
 * Class CoreSettingsForm.
 */
class GrouperSettingsForm extends ConfigFormBase
{

    use GrouperHelperTrait;

    /**
     * Constructs a new GrouperSettingsForm object.
     */
    public function __construct(ConfigFactoryInterface $config_factory)
    {
        parent::__construct($config_factory);
    }

    /**
     *
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'grouper_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
          'basicshib.grouper_settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('basicshib.grouper_settings');

        $array = $this->getDrupalRoles();

        $form['roles'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Roles'),
        ];

        for ($i = 0; $i < sizeof($array); $i++) {
            $form['roles']['list' . $i] = [
            '#type' => 'textfield',
            '#title' => $this->t($array[$i]),
            '#description' => $this->t('groups assigned to this role'),
            '#default_value' => $config->get('role_' . $i),
            ];
        } 

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        $config = $this->config('basicshib.grouper_settings');
        $array = $this->getDrupalRoles();

        for ($i = 0; $i < sizeof($array); $i++) {
            $this->config('basicshib.grouper_settings')
                ->set('role_' . $i, $form_state->getValue('list' . $i))
                ->save();
        }
    }

}
