<?php

namespace Drupal\basicshib;

use Drupal\basicshib\Exception\AttributeException;
use Drupal\basicshib\Exception\AuthenticationException;
use Drupal\basicshib\Plugin\AuthFilterPluginInterface;
use Drupal\basicshib\Plugin\BasicShibPluginManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Url;
use Drupal\Core\Routing\RedirectDestination;

/**
 *
 */
class AuthenticationHandler implements AuthenticationHandlerInterface
{
    /**
     * Instance variable attribute mapper
     *
     * @var AttributeMapperInterface
     */
    private $_attribute_mapper;
    /**
     * Instance variable session tracker
     *
     * @var SessionTracker
     */
    private $_session_tracker;
    /**
     * Instance variable user provider
     *
     * @var \Drupal\basicshib\Plugin\UserProviderPluginInterface
     */
    private $_user_provider;
    /**
     * Instance variable auth filter 
     *
     * @var \Drupal\basicshib\Plugin\AuthFilterPluginInterface[]
     */
    private $_auth_filters = [];
    /**
     * Instance variable handlers
     *
     * @var array
     */
    private $_handlers = [];
    /**
     * Instance variable path validator
     *
     * @var \Drupal\Core\Path\PathValidatorInterface
     */
    private $_path_validator;
    /**
     * Instance variable redirect destination
     *
     * @var \Drupal\Core\Routing\RedirectDestination
     */
    private $_redirect_destination;
    /**
     * AuthenticationHandler constructor.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
     * @param AttributeMapperInterface $_attribute_mapper
     * @param \Drupal\basicshib\Plugin\BasicShibPluginManager $_user_provider_plugin_manager
     * @param \Drupal\basicshib\Plugin\BasicShibPluginManager $auth_filter_plugin_manager
     *
     * @throws \Drupal\Component\Plugin\Exception\PluginException
     */
    public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack, AttributeMapperInterface $attribute_mapper, BasicShibPluginManager $user_provider_plugin_manager, BasicShibPluginManager $auth_filter_plugin_manager, PathValidatorInterface $path_validator, RedirectDestination $redirect_destination)
    {
        $this->_session_tracker 
            = new SessionTracker($request_stack->getCurrentRequest()->getSession());
        $plugins = $config_factory->get('basicshib.settings')->get('plugins');
        $this->_attribute_mapper = $attribute_mapper;
        $this->_user_provider = $user_provider_plugin_manager
            ->createInstance($plugins['user_provider']);
        foreach ($plugins['auth_filter'] as $name) {
            $this->_auth_filters[$name] 
                = $auth_filter_plugin_manager->createInstance($name);
        }

        $this->_handlers 
            = $config_factory->get('basicshib.settings')->get('handlers');
        $this->_path_validator = $path_validator;
        $this->_redirect_destination = $redirect_destination;
    }

    /** 
     * {@inheritDoc}
     */
    public function authenticate()
    {
        // Get key attributes.
        $key_attributes = $this->_getKeyAttributes();
        // Load the user account.
        $account = $this->_user_provider->loadUserByName($key_attributes['name']);
        if ($account) {
            $this->_assertExistingUserLoginAllowed($account);
        } else {
            $this->_assertUserCreationAllowed($key_attributes['name']);
            // Create user.
            $account = $this->_user_provider->createUser($key_attributes['name'], $key_attributes['mail']);
        }

        $this->_saveAccount($account, $key_attributes);
        $this->_session_tracker->set($key_attributes['session_id']);
        $this->_userLoginFinalize($account);
        // Added 7/23 by code+.
        return $account;
    }

    /**
     * Save accoutn function
     *
     * @param \Drupal\user\UserInterface $account
     * @param array $key_attributes
     *
     * @throws \Drupal\basicshib\Exception\AuthenticationException
     * 
     * @return saved account
     */
    private function _saveAccount(UserInterface $account, array $key_attributes)
    {
        $message = $account->isNew() ? 'Saving new account for \'%s\' has failed' : 'Updating existing account for \'%s\' has failed';
        $code = $account->isNew() ? AuthenticationException::USER_CREATION_FAILED : AuthenticationException::USER_UPDATE_FAILED;
        if ($account->isNew()) {
            try {
                $account->save();
            }
            catch (EntityStorageException $exception) {
                throw new AuthenticationException(sprintf($message, $account->getAccountName()), $code, $exception);
            }
        }
    }

    /**
     * Assert that an existing user is allowed to log in.
     *
     * @param \Drupal\user\UserInterface $account
     *
     * @throws \Drupal\basicshib\Exception\AuthenticationException
     */
    private function _assertExistingUserLoginAllowed(UserInterface $account)
    {
        if ($account->isBlocked()) {
            throw new AuthenticationException(sprintf('User \'%s\' is blocked and cannot be authenticated', $account->getAccountName()), AuthenticationException::USER_BLOCKED);
        }

        foreach ($this->_auth_filters as $auth_filter) {
            if (!$auth_filter->isExistingUserLoginAllowed($account)) {
                throw new AuthenticationException($auth_filter->getError(AuthFilterPluginInterface::ERROR_EXISTING_NOT_ALLOWED, $account), AuthenticationException::LOGIN_DISALLOWED_FOR_USER);
            }
        }
    }

    /**
     * Assert that user creation is allowed.
     *
     * @throws \Drupal\basicshib\Exception\AuthenticationException
     */
    private function _assertUserCreationAllowed($name)
    {
        foreach ($this->_auth_filters as $auth_filter) {
            if (!$auth_filter->isUserCreationAllowed()) {
                $message = $auth_filter->getError(AuthFilterPluginInterface::ERROR_CREATION_NOT_ALLOWED);
                throw new AuthenticationException(sprintf('%s, user=\'%s\'', $message, $name), AuthenticationException::USER_CREATION_NOT_ALLOWED);
            }
        }
    }

    /**
     * Get the key attributes 'session_id', 'name', and 'mail'
     *
     * @return array
     *   An associative array whose keys are:
     *   - session_id
     *   - name
     *   - mail
     *
     * @throws \Drupal\basicshib\Exception\AuthenticationException
     */
    private function _getKeyAttributes()
    {
        $attributes = [];
        foreach (['session_id', 'name', 'mail'] as $id) {
            try {
                $attributes[$id] = $this->_attribute_mapper->getAttribute($id, false);
            }
            catch (AttributeException $exception) {
                throw new AuthenticationException(sprintf('Missing required attribute \'%s\'', $id), AuthenticationException::MISSING_ATTRIBUTES, $exception);
            }
        }

        return $attributes;
    }

    /**
     * Checks user session and logs the user out if the session is not valid.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Drupal\Core\Session\AccountProxyInterface $account
     *
     * @return int
     */
    public function checkUserSession(Request $request, AccountProxyInterface $account)
    {
        // Handle anonymous user.
        if ($account->isAnonymous()) {
            if ($this->_session_tracker->exists()) {
                $this->_session_tracker->clear();
                return self::AUTHCHECK_LOCAL_SESSION_EXPIRED;
            }
            return self::AUTHCHECK_IGNORE;
        }

        // Authenticated user who is not authenticated via shibboleth.
        if (!$this->_session_tracker->exists()) {
            return self::AUTHCHECK_IGNORE;
        }

        // Authenticated user with expired shib session.
        $session_id = $this->_attribute_mapper->getAttribute('session_id', true);
        if (!$session_id) {
            $this->_terminateSession($account);
            return self::AUTHCHECK_SHIB_SESSION_EXPIRED;
        }

        // Authenticated user whose tracked session id does not match the current
        // Session id.
        if ($session_id !== $this->_session_tracker->get()) {
            $this->_terminateSession($account);
            return self::AUTHCHECK_SHIB_SESSION_ID_MISMATCH;
        }

        // Additional checks by auth filter plugins.
        foreach ($this->_auth_filters as $auth_filter) {
            $value = $auth_filter->checkSession($request, $account);
            if ($value !== self::AUTHCHECK_IGNORE) {
                $this->_terminateSession($account);
                return $value;
            }
        }

        return self::AUTHCHECK_IGNORE;
    }

    /**
     *
     *
     * @param \Drupal\Core\Session\AccountProxyInterface $account
     */
    private function _terminateSession(AccountProxyInterface $account)
    {
        $this->_session_tracker->clear();
        user_logout();
    }

    /**
     * Finalize login.
     *
     * @param \Drupal\user\UserInterface $account
     */
    private function _userLoginFinalize(UserInterface $account)
    {
        user_login_finalize($account);
    }

    /**
     * {@inheritDoc}
     * 
     * @return String login URL
     */
    public function getLoginUrl()
    {
        $login_handler = $this->_handlers['login'];
        if (!$target = $this->_redirect_destination->get()) {
            $target = '/user';
        }

        $login_url = Url::fromUserInput($login_handler);
        if (!$login_url) {
            // @todo Log something about this?
            return 'login_handler_not_valid';
        }

        $target_url = $this->_path_validator->getUrlIfValid('/basicshib/login');
        $target_url->setAbsolute(true);
        $target_url->setOption('query', ['after_login' => $target]);
        $login_url->setOption('query', ['target' => $target_url->toString()]);
        return $login_url->toString();
    }

}
