<?php
/**
 * BasicShibGrouper File Doc Comment
 *
 * @category LoginController
 *
 */
namespace Drupal\basicshib\Controller;

use Drupal\basicshib\AuthenticationHandlerInterface;
use Drupal\basicshib\AuthorizationHandlerInterface;
use Drupal\basicshib\Exception\AuthenticationException;
use Drupal\basicshib\Exception\RedirectException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class LoginController.
 */
class LoginController extends ControllerBase {
    /**
     * Path validator instance
     * 
     * @var \Drupal\Core\Path\PathValidatorInterface
     */
    protected $path_validator;

    /**
     * Immutable Config instance
     * 
     * @var \Drupal\Core\Config\ImmutableConfig
     */
    protected $configuration;

    /**
     * AuthenticationHandler instance.
     * 
     * @var \Drupal\basicshib\AuthenticationHandlerInterface
     */
    protected $authentication_handler;

    /**
     * AuthorizationHandler instance.
     * 
     * @var \Drupal\basicshib\AuthorizationHandlerInterface
     */
    protected $authorization_handler;

    /**
     * Request instance
     * 
     * @var null|Request
     */
    protected $request;

    /**
     * True if Grouper plugin enabled, false otherwise.
     * 
     * @var bool
     */
    protected $grouper_on;

    /**
     * Constructs a new LoginController object.
     *
     * @param \Drupal\basicshib\AuthenticationHandlerInterface $authentication_handler
     * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
     * @param \Drupal\basicshib\AuthorizationHandlerInterface $authorization_handler
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     */
    public function __construct(
        AuthenticationHandlerInterface $authentication_handler,
        RequestStack $request_stack,
        ConfigFactoryInterface $config_factory,
        PathValidatorInterface $path_validator,
        AuthorizationHandlerInterface $authorization_handler
    ) {
        $this->request = $request_stack
            ->getCurrentRequest();

        $this->configuration = $config_factory
            ->get('basicshib.settings');

        // Checking whether the grouper plugin is enabled (call authorize
        // only if grouepr plugin is enabled)
        $plugin_settings = $config_factory
            ->get('basicshib.settings')
            ->get('plugin_enabled');

        $this->grouper_on = $plugin_settings['grouper_enabled'];

        $this->path_validator = $path_validator;

        $this->authentication_handler = $authentication_handler;

        if ($this->grouper_on) {
            $this->authorization_handler = $authorization_handler;
        }
    }

    /**
     * {@inheritdoc}
     * 
     * @param ContainerInterface $container 
     * 
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * 
     * @return a new instance of LoginController
     */
    public static function create(ContainerInterface $container)
    {
        /** 
         * Returning a new loginController instance 
         */
        return new static(
            $container->get('basicshib.authentication_handler'),
            $container->get('request_stack'),
            $container->get('config.factory'),
            $container->get('path.validator'),
            $container->get('basicshib.authorization_handler')
        );
    }

    /**
     * Login.
     *
     * @return array|RedirectResponse
     *   Either a render array or a redirect response.
     */
    public function login() 
    {
        $exception = null;

        $messages = $this->configuration->get('messages');

        try {
            // Set the redirect response first so authentication doesn't get
            // processed when the redirect is invalid.
            $redirect_response = $this->_getRedirectResponse();

            $current_account = $this->authentication_handler->authenticate();

            if ($this->grouper_on) {
                $this->authorization_handler->authorize($current_account);
            }

            return $redirect_response;
        }
        catch (RedirectException $exception) {
            print_r("exception");
            switch ($exception->getCode()) {
            case RedirectException::BLOCKED_EXTERNAL:
                return ['#markup' => $messages['external_redirect_error']];

            case RedirectException::INVALID_PATH:
                return ['#markup' => $this->t('Invalid redirect path.')];

            default:
                return ['#markup' => $this->t('An unknown redirect error occurred')];
            }

        }
        catch (AuthenticationException $exception) {
            switch ($exception->getCode()) {
            case AuthenticationException::LOGIN_DISALLOWED_FOR_USER:
                return ['#markup' => $messages['login_disallowed_error']];

            case AuthenticationException::USER_BLOCKED:
                return ['#markup' => $messages['account_blocked_error']];

            case AuthenticationException::USER_CREATION_NOT_ALLOWED:
                return ['#markup' => $messages['user_creation_not_allowed_error']];

            default:
                return ['#markup' => $messages['generic_login_error']];
            }
        }
        finally {
            if ($exception !== null) {
                $this->getLogger('basicshib')->error(
                    'Authentication failed: @message -- @backtrace_string',
                    Error::decodeException($exception)
                );
            }
        }
    }

      /**
       * Get the redirect response.
       *
       * @return \Symfony\Component\HttpFoundation\RedirectResponse
       *
       * @throws \Drupal\basicshib\Exception\RedirectException
       */
    private function _getRedirectResponse() 
    {
        $redirect_path = $this->request
            ->query
            ->get('after_login');

        if ($redirect_path === null) {
            $redirect_path = $this->configuration
                ->get('default_post_login_redirect_path');
        }

        $url = $this->path_validator
            ->getUrlIfValidWithoutAccessCheck($redirect_path);

        if ($url === false) {
            throw new RedirectException(
                $this->t(
                    'Redirect path @path is not valid',
                    ['@path' => $redirect_path]
                ), 
                RedirectException::INVALID_PATH
            );
        }

        if ($url->isExternal()) {
            throw new RedirectException(
                $this->t(
                    'Blocked attempt to redirect to external url @url',
                    ['@url' => $redirect_path]
                ), 
                RedirectException::BLOCKED_EXTERNAL
            );
        }

        try {
            return new RedirectResponse($redirect_path);
        }
        catch (\InvalidArgumentException $exception) {
            throw new RedirectException(
                $this->t(
                    'Error creating redirect: @message',
                    ['@message' => $exception->getMessage()]
                ), 
                RedirectException::INVALID_PATH, $exception
            );
        }
    }
}
